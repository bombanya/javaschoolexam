package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
        int iterX = 0;
        int iterY = 0;
        while (iterX < x.size() && iterY < y.size()){
            if (y.get(iterY).equals(x.get(iterX))) iterX++;
            iterY++;
        }
        return iterX == x.size();
    }
}
