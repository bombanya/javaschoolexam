package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null) throw new CannotBuildPyramidException();
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        if (inputNumbers.size() == 0) throw new CannotBuildPyramidException();
        int rows = (int) Math.sqrt(1 + 8 * inputNumbers.size());
        if (Math.pow(rows, 2) != (1 + 8 * inputNumbers.size())) throw new CannotBuildPyramidException();
        rows = (rows - 1) / 2;
        int[][] res = new int[rows][rows * 2 - 1];
        inputNumbers.sort(Integer::compareTo);
        for (int i = 0, elem = 0, startPlace = res[0].length / 2; i < rows; i++, startPlace--){
            for (int j = 0, place = startPlace; j <= i; j++, place += 2, elem++)
                res[i][place] = inputNumbers.get(elem);
        }
        return res;
    }

}
