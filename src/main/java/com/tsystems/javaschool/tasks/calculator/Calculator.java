package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Calculator {

    private int index;
    private String statement;
    private final DecimalFormat formatter = new DecimalFormat("#.####", new DecimalFormatSymbols(Locale.US));

    {
        formatter.setRoundingMode(RoundingMode.HALF_UP);
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        index = 0;
        this.statement = statement;
        if (statement == null) return null;
        try {
            double res = parsePlusMinusLevel();
            if (index == statement.length()) return formatter.format(res);
            else return null;
        } catch (IncorrectStatementException e) {
            return null;
        }
    }

    private boolean checkCharAndGetNext(char ch){
        while (index != statement.length() && statement.charAt(index) == ' ') index++;
        if (index == statement.length() || statement.charAt(index) != ch) return false;
        index++;
        while (index != statement.length() && statement.charAt(index) == ' ') index++;
        return true;
    }

    private double parsePlusMinusLevel() throws IncorrectStatementException {
        double exp = parseMulDivLevel();
        for (;;){
            if (checkCharAndGetNext('+')) exp += parseMulDivLevel();
            else if (checkCharAndGetNext('-')) exp -= parseMulDivLevel();
            else return exp;
        }
    }

    private double parseMulDivLevel() throws IncorrectStatementException{
        double exp = parseFinally();
        for (;;){
            if (checkCharAndGetNext('*')) exp *= parseFinally();
            else if (checkCharAndGetNext('/')){
                double right = parseFinally();
                if (right == 0) throw new IncorrectStatementException(index);
                exp /= right;
            }
            else return exp;
        }
    }

    private double parseFinally() throws IncorrectStatementException{
        if (checkCharAndGetNext('(')){
            double fromBrackets = parsePlusMinusLevel();
            if (!checkCharAndGetNext(')')) throw new IncorrectStatementException(index);
            return fromBrackets;
        }
        else{
            int doubleStart = index;
            boolean doubleFlag = false;
            while (index != statement.length() &&
                    ((statement.charAt(index) >= '0' && statement.charAt(index) <= '9')
                            || statement.charAt(index) == '.')){
                if (statement.charAt(index) == '.'){
                    if (doubleFlag) break;
                    else doubleFlag = true;
                }
                index++;
            }
            if (doubleStart != index) return Double.parseDouble(statement.substring(doubleStart, index));
            throw new IncorrectStatementException(index);
        }
    }

    static class IncorrectStatementException extends Exception{
        IncorrectStatementException(int index){
            super("Error at " + index);
        }
    }

}
